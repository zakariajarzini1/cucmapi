package com.example.cucmspringApp.config;

import com.example.cucmspringApp.models.User;
import com.example.cucmspringApp.models.jwtAuth.UserRole;
import com.example.cucmspringApp.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class InitDataBase implements CommandLineRunner {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void run(String... args) throws Exception {

        if(userRepository.count() == 0) {
            PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

            User admin1 = new User("zakaria", passwordEncoder.encode("zakaria@1997"), UserRole.ADMIN, true);

            User admin2 = new User("zineb", passwordEncoder.encode("zineb@1995"), UserRole.ADMIN, true);

            userRepository.save(admin1);
            userRepository.save(admin2);
            System.out.println("data base initialized");
        }
    }
}
