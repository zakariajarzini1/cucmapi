package com.example.cucmspringApp.repositories;


import com.example.cucmspringApp.models.CucmUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CucmUserRepository extends JpaRepository<CucmUser, Long> {

}
