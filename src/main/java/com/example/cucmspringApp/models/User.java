package com.example.cucmspringApp.models;

import com.example.cucmspringApp.models.jwtAuth.UserRole;
import com.sun.istack.NotNull;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class User {
    @Id
    //@NotBlank(message = "Username is required")
    //@Size(min = 3, max = 20, message = "invalid usernam !")
    @NotNull
    private String username;

    //@Size(min = 4, max = 30, message = "Password must be between 4 and 30 characters long")
    //@NotBlank(message = "Password is required")
    @NotNull
    private String password;
    @NotNull
    private UserRole role;
    private Boolean enabled = false;

    public User() {
    }

    public User(String username, String password, UserRole role, Boolean enabled) {
        this.username = username;
        this.password = password;
        this.role = role;
        this.enabled = enabled;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
