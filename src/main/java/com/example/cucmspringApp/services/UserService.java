package com.example.cucmspringApp.services;

import com.example.cucmspringApp.handlers.UsernameAlreadyExistException;
import com.example.cucmspringApp.models.User;
import com.example.cucmspringApp.models.jwtAuth.UserRole;
import com.example.cucmspringApp.repositories.UserRepository;
import com.example.cucmspringApp.services.security.JwtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JwtService jwtService;


    public Optional<User> getUserByUsername(String username){
        return userRepository.findByUsername(username);
    }

    public List<User> getAllUsers(){
        List<User> users = new ArrayList<>();
        userRepository.findAll().forEach(users::add);
        return users;
    }

    public User addUser(User user) throws UsernameAlreadyExistException, IllegalArgumentException{
        if(isExist(user.getUsername()))
            throw new UsernameAlreadyExistException("username is already in use !!");

        if(user.getRole() == null)
            user.setRole(UserRole.USER);

        if(!user.getRole().toString().equals("USER") && !user.getRole().toString().equals("MANAGER"))
            throw new IllegalArgumentException ("role should be 'USER' or 'MANAGER'");

        user.setPassword(passwordEncoder().encode(user.getPassword()));
//        if(accCreation)
//            user.setEnabled(false);
        user.setEnabled(user.getRole().toString().equals("USER"));
        return userRepository.save(user);
    }




//    @Transactional(rollbackFor = Throwable.class, propagation = Propagation.REQUIRED)
//    public void updateUser(User newUser, String token)throws UsernameAlreadyExistException{
//        if (token != null && token.startsWith("Bearer ")) {
//            token = token.substring(7);
//
//            String username = jwtService.getUsernameFromToken(token);
//
//            Optional<User> userOptional = getUserByUsername(username);
//            User user = userOptional
//                    .orElseThrow(() -> new UsernameNotFoundException("No user Found with username : "
//                            + username));
//
//            // cant change role or enabled values
//            newUser.setEnabled(user.getEnabled());
//            newUser.setRole(user.getRole());
//
//            if(newUser.getUsername().equals(username)) {
//                newUser.setPassword(passwordEncoder().encode(newUser.getPassword()));
//                userRepository.save(newUser);
//            }else{
//                addUser(newUser, false);
//                userRepository.deleteById(username);
//            }
//        }else{
//            System.out.println("invalid token");
//        }
//    }

    public void deleteUserById(String username){
        userRepository.deleteById(username);
    }

    public Boolean isExist(String username){
        return userRepository.existsById(username);
    }

    public void activateUserAcc(String username) {
        Optional<User> userOptional = this.getUserByUsername(username);
        User user = userOptional
                .orElseThrow(() -> new UsernameNotFoundException("No user Found with username : "
                        + username));

        user.setEnabled(true);
        userRepository.save(user);
    }
}
